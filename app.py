from flask import Flask,jsonify
from flask_restful import Resource,reqparse, Api
from flask_mongoengine import MongoEngine

app = Flask(__name__)
api = Api(app)

app.config['MONGODB_SETTINGS'] = {
    'db': 'student_management',
    'host' : 'localhost',
    'port' : 27017   
}
db = MongoEngine()
db.init_app(app)

class StudentModel(db.Document):
    _id = db.IntField()
    student_name = db.StringField(required=True)
    student_email = db.StringField(required=True)

task_insert = reqparse.RequestParser() # flask know that client send data to the server
task_insert.add_argument("student_name", type=str, help="student_name is required",required=True) #add arugments, help=show the error
task_insert.add_argument("student_email", type=str, help="student_email is required",required=True)

task_update = reqparse.RequestParser()
task_update.add_argument("student_name",type=str)
task_update.add_argument("student_email",type=str)

class StudentAll(Resource):
    def get(self):
        output = []
        task = StudentModel.objects.all()
        for i in task:
            output.append(i)
        return jsonify(output)

class StudentOne(Resource):
    def get(self,student_id):
        data = StudentModel.objects.get(_id=student_id)
        return jsonify(data)
    
    def post(self,student_id):
        args = task_insert.parse_args() #get data to the server
        data = StudentModel(_id=student_id, student_name=args["student_name"], student_email=args["student_email"]).save()
        return jsonify(data)

    def put(self,student_id):
        args = task_update.parse_args()
        if args['student_name']:
            StudentModel.objects.filter(_id=student_id).update(student_name=args['student_name'])
        if args['student_email']:
            StudentModel.objects.filter(_id=student_id).update(student_email=args['student_email'])
        return "Student id:{} Updated Successfully".format(student_id)

    def delete(self, student_id):
        StudentModel.objects.filter(_id=student_id).delete()
        return "delete"

api.add_resource(StudentAll, '/student')
api.add_resource(StudentOne,'/student/<student_id>')

app.run()